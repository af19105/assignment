import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class assignment {
    private JPanel root;
    private JButton udonbutton;
    private JButton gyouzaButton;
    private JButton karaageButton;
    private JButton tempraButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JButton mazesobaButton;
    private JButton pizzaButton;
    private JLabel orderedItemsLabel;
    private JLabel TopLabel;
    private JLabel totalLabel;

    int total = 0;

    void order(String food, int value) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food +" "+ value + " yen\n");
            total += value;
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
            totalLabel.setText("Total" + "   " + total + " " + "yen");
        }
    }

    public assignment() {
        udonbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 300);

            }
        });
        udonbutton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpg")));

        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyouza", 200);

            }
        });
        gyouzaButton.setIcon(new ImageIcon(this.getClass().getResource("gyouza.jpg")));

        tempraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempra", 150);
            }
        });
        tempraButton.setIcon(new ImageIcon(this.getClass().getResource("tempra.jpg")));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 220);

            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("karaage.jpg")));

        mazesobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mazesoba", 600);
            }
        });
        mazesobaButton.setIcon(new ImageIcon(this.getClass().getResource("mazesoba.jpg")));

        pizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza", 700);
            }
        });
        pizzaButton.setIcon(new ImageIcon(this.getClass().getResource("pizza.jpg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    String selectvalues[] = {"Cash", "Credit card", "Transportation IC", "PayPay"};
                    int method=JOptionPane.showOptionDialog(null,
                            "What is the payment method？",
                            "What is the payment method？",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            selectvalues,
                            selectvalues[0]
                    );
                    JOptionPane.showMessageDialog(null,
                            "Thank you for using this store!\nThe total amount is "
                                    + total
                                    + " yen.\nPayment is accepted by " + selectvalues[method]+".");
                    total = 0;
                    totalLabel.setText("Total" + "   " + total + " " + "yen");
                    textPane1.setText(" ");
                }

            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("assignment");
        frame.setContentPane(new assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}



